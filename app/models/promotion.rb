class Promotion < ApplicationRecord
  belongs_to :shop
  has_many :orders, through: :order_promotions
  belongs_to :action, polymorphic: true
  has_many :rules
end
