class CouponGift < ApplicationRecord
  belongs_to :promotion
  has_many :promotions, as: :action
end
