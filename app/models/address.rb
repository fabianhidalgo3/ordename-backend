class Address < ApplicationRecord
  belongs_to :user
  belongs_to :territory

  geocoded_by :address

  reverse_geocoded_by :latitude, :longitude

  RADIUS_IN_KILOMETERS = {
    default: 1,
    max: 2
  }.freeze
end
