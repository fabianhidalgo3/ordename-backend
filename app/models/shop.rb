class Shop < ApplicationRecord
  belongs_to :address
  belongs_to :business
  has_many :products, through: :stocks

  # shop.forbidden_sellers => Tiendas en las que no puede comprar
  has_many :shop_forbidden_sellers, foreign_key: "buyer_id", class_name: "ForbiddenShop"
  has_many :forbidden_sellers, through: :shop_forbidden_sellers, source: :seller

  # shop.forbidden_shop_buyers => Tiendas a las que no les puede vender
  has_many :shop_forbidden_buyers, foreign_key: "seller_id", class_name: "ForbiddenShop"
  has_many :forbidden_shop_buyers, through: :shop_forbidden_buyers, source: :buyer

  # shop.forbidden_users => Usuarios a los que no les puede vender
  has_many :shop_forbidden_users, foreign_key: "seller_id", class_name: "ForbiddenShop"
  has_many :forbidden_users, through: :shop_forbidden_users, source: :buyer
  
  # Explicación: https://stackoverflow.com/questions/25493368/many-to-many-self-join-in-rails
                                                   
end
