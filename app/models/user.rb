class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :trackable, :jwt_authenticatable,jwt_revocation_strategy: Devise::JWT::RevocationStrategies::Null
    has_many :orders
    has_many :addresses
    has_many :businesses, through: :employees

    # user.forbidden_sellers => Tiendas en las que no puede comprar
    has_many :user_forbidden_sellers, foreign_key: "buyer_id", class_name: "ForbiddenShop"
    has_many :forbidden_sellers, through: :user_forbidden_sellers, source: :seller 
    def generate_jwt
      JWT.encode({ id: id,
                  exp: 60.days.from_now.to_i },
                 Rails.application.secret_key_base)
    end

end
