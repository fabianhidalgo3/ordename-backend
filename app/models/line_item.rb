class LineItem < ApplicationRecord
  belongs_to :order
  belongs_to :product
  belongs_to :seller, class_name: "Shop"

  has_many :refunds
end
