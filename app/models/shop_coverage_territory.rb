class ShopCoverageTerritory < ApplicationRecord
  belongs_to :territory
  belongs_to :shop
end
