class ProductDiscount < ApplicationRecord
  belongs_to :product
  has_many :promotions, as: :action
end
