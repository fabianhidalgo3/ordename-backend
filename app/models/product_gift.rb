class ProductGift < ApplicationRecord
  belongs_to :product
  has_many :promotions, as: :action
end
