class ShipmentDiscount < ApplicationRecord
    has_many :promotions, as: :action
end
