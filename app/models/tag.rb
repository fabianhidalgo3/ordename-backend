class Tag < ApplicationRecord
  belongs_to :parent, class_name: 'Tag', optional: true
  has_many :product_tags
  has_many :products, through: :product_tags
end
