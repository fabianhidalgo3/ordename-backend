class TerritoryType < ApplicationRecord
  belongs_to :country
  has_many :territories
end
