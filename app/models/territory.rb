class Territory < ApplicationRecord
  belongs_to :parent, optional: true
  belongs_to :territory_type
  has_one :country, through: :territory_type
end
