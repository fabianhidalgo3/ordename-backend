class Product < ApplicationRecord
    has_many :taxes, through: :product_taxes
    has_many :tags, through: :product_tags
    has_many :stocks
    has_many :shops, through: :stocks
    has_many :discounts, class_name: "ProductDiscount"
    has_many :gifts, class_name: "ProductGifts"
    has_many :line_items
    has_many :orders, through: :line_items
end
