class Country < ApplicationRecord
    has_many :territory_types
    has_many :territories, through: :territory_types
end
