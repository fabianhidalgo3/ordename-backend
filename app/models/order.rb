class Order < ApplicationRecord
  belongs_to :user
  belongs_to :buyer_shop, class_name: "Shop"
  belongs_to :address, optional: true
  has_many :line_items
  has_many :products, through: :line_items
end
