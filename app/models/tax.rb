class Tax < ApplicationRecord
    has_many :products, through: :product_taxes
end
