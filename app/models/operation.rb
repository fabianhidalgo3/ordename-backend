class Operation < ApplicationRecord
  belongs_to :operator
  has_many :rules
  has_many :promotions, through: :rules
end
