class Price < ApplicationRecord
  belongs_to :seller, class_name: "Shop"
  belongs_to :buyer_shop, class_name: "Shop"
  belongs_to :buyer_user, class_name: "User"
  belongs_to :product
  belongs_to :territory
end
