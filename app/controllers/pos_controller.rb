

class PosController < ApplicationController

    before_action :authenticate_user, except: [:accept_user]

    def get_orders
        user = User.find(@current_user_id)
        page, size, startDate, endDate = get_orders_params
        url = "https://lab.random.cl/hub/hub/proxy/documentos/detalle?idAppHub=pos&bridgeModeHub=true&order=-FEEMDO&order=-HORAGRAB&tido=BLV,FCV"
        response = Faraday.get(url) do |req|
            req.headers['Authorization'] = "Bearer " + user.hub_token
            req.params['page'] = page
            req.params['size'] = size
            req.params['fecha_desde'] = startDate
            req.params['fecha_hasta'] = endDate
        end
        render json: response.body
    end

    def create_order
        user = User.find(@current_user_id)
        lines, total, paymentMethod, bank, voucher = create_order_params
        body = {
            datos: {
              empresa: "01",
              codigoEntidad: "BOLETA",
              tido: "BLV",
              modalidad: "WEB",
              lineas: lines,
              pagos: [
                {
                  tipoPago: paymentMethod,
                  monto: total.to_i,
                  banco:bank,
                  voucher: voucher
                },
              ],
            }}
        response = Faraday.post('https://lab.random.cl/hub/hub/proxy/web32/documento?idAppHub=pos&bridgeModeHub=true') do |req|
            req.body = body.to_json
            req.headers['Content-Type'] = "application/json"
            req.headers['Authorization'] = "Bearer " + user.hub_token
        end
        render json: response.body
    end


    def get_dashboard_info
        user = User.find(@current_user_id)
        startDate, endDate = dashboard_params
        response = Faraday.get("https://lab.random.cl/hub/hub/proxy/gestion/documentos/pagos?idAppHub=pos&bridgeModeHub=true&grouping=tidp&tido=BLV,FCV") do |req|
            req.params['fecha_desde'] = startDate
            req.params['fecha_hasta'] = endDate
            req.headers['Authorization'] = "Bearer " + user.hub_token
        end
        render json: response.body
    end


    def get_order
        id = get_order_params
        user = User.find(@current_user_id)
        response = Faraday.get("https://lab.random.cl/hub/hub/proxy/documentos/render?idAppHub=pos&bridgeModeHub=true") do |req|
            req.params["idmaeedo"] = id
        end
        render json: response.body
    end

    # accepts user in pos, key header must be the same as the api key we have
    def accept_user
        api_key = request.headers["API-KEY"]
        if api_key == Rails.application.credentials[Rails.env.to_sym][:acepta][:api_key]
            rut = params.require(:rut_usuario)
            if(rut) == "11111111-1"
                render json: { message: "user accepted correctly"}, status: :ok
                return
            end
            user = User.find_by_dni_number(params.require(:rut_usuario))
            user.pos_accepted = true
            if user.save()
                render json: { message: "user accepted"}, status: :ok
            else
                render json: { errors: "user could not be accepted" }, status: :unprocessable_entity
            end
        else
            render json: {errors: "API KEY not present or incorrect"}, status: :unprocessable_entity
        end

    end

    def user_accepted
        user = User.find(@current_user_id)
        render json: {Accepted: user.pos_accepted}, status: :ok

    end
    
    def get_order_params
        params.require(:id)
    end

    def dashboard_params
        params.require([:startDate, :endDate])
    end

    def get_orders_params
        params.require([:page, :size, :startDate, :endDate])
    end

    def create_order_params
        params.require([:lines, :total, :paymentMethod, :bank, :voucher])
    end
end
