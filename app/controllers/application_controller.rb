class ApplicationController < ActionController::API
    include ActionController::MimeResponds
    include ActionController::HttpAuthentication::Basic::ControllerMethods
    include ActionController::HttpAuthentication::Token::ControllerMethods

    # receives bearer token and authenticates user. Use @current_user_id to find user in other requests
    def authenticate_user
        if request.headers['Authorization'].present?
          authenticate_or_request_with_http_token do |token|
            begin
              jwt_payload = JWT.decode(token, Rails.application.secret_key_base).first
    
              @current_user_id = jwt_payload['id']
            rescue JWT::ExpiredSignature, JWT::VerificationError, JWT::DecodeError
              head :unauthorized
            end
          end
        else
          head :unauthorized
        end
    end
end
