# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]

  #GET /resource/sign_in
  # def new
  #   puts "new session"
  #   super
  # end


  def create
    app = request.headers["app"]
    password = ''
    user = nil
    userB2c = nil

    if app == "pos"
      username, password = login_params_pos
      user = User.find_by_email(username)
    end
    if app == "b2c"
      userB2c = User.find_by_phone(login_params_b2c)
    end

    if !user.nil? && user.valid_password?(password)
      @current_user = user
      # behaviour depending on what app called it (pos, b2b, b2c)
      # in POS, the auth token from Random is saved in the user.
      body = {username: username, password: password}
      response = Faraday.post('https://lab.random.cl/hub/login') do |req|
        req.body = body
        req.headers['content_type'] = 'application/json'
        req.headers['accept'] = 'application/json'
      end
      if response.status == "200"
        data = JSON.parse(response)
        user = User.find_by_email(username)
        if user != nil
            user.hub_token = data["token"]
            user.save()
        end
      end
    # TODO: return active token if any
    token = {:token => user.generate_jwt}
    elsif !userB2c.nil?
      message = ""

      if params.has_key?(:sms_code)
        user_validation = UserValidation.find_by(phone: params["phone"])
        if user_validation.expires_at >= Time.current
          if user_validation.sms_code == params["sms_code"]
            render json: {
              success: true,
              message: "Done.",
              user: userB2c,
              token: userB2c.generate_jwt
            }
            return
          else
            message = "SMS Code doesn't match."
          end
        else
          message = "SMS Code expired at: #{user_validation.expires_at}"
        end
      else
        message = "Missing SMS Code."
      end

      render json: {
        success: false,
        message: message,
        data: nil
      },status: :unprocessable_entity

    else
      render json: {
        success: false,
        message: "User doesn't exist.",
        data: nil
      },status: :unprocessable_entity
    end
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end


  private
    def login_params_pos
      params.require([:username, :password])
    end

    def login_params_b2c
      params.require(:phone)
    end
   
end
