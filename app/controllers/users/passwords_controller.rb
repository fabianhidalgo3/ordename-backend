# frozen_string_literal: true

class Users::PasswordsController < Devise::PasswordsController
  before_action :authenticate_user
  before_action :set_user, only: [:update]
  # GET /resource/password/new
  # def new
  #   super
  # end

  # POST /resource/password
  # def create
  #   super
  # end

  # GET /resource/password/edit?reset_password_token=abcdef
  # def edit
  #   super
  # end

  # PUT /resource/password
  def update
    message = ""
    success_state = false

    if @user.valid_password?(params["current_password"])
      if params["password"] == params["password_confirmation"]
        @user.update(password: params["password"])

        message = "Password changed"
        success_state = true
      else
        message = "Password doesn't match"
      end
    else
      message = "Invalid password"
    end

    render json: {
      success: success_state,
      message: message
    }
  end


  private
    def set_user
      @user = User.find(@current_user_id)
    end

  # protected

  # def after_resetting_password_path_for(resource)
  #   super(resource)
  # end

  # The path used after sending reset password instructions
  # def after_sending_reset_password_instructions_path_for(resource_name)
  #   super(resource_name)
  # end
end
