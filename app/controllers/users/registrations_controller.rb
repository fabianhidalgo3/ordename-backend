# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  #GET /resource/sign_up
  #def new
    #super
  #end

  # POST /resource
  def create
    app = request.headers["app"]
    message = ""

    if app == "b2c"
      user_validation = UserValidation.find_by(phone: params["data"]["phone"])
      if user_validation
        if user_validation.validated_at.nil?
          message = "Validate cell phone first."
        end
      else
        message = "The cell phone number is not validated."
      end

      if !message.empty?
        render json: {
          success: false,
          message: message,
          data: nil
        },status: :unprocessable_entity
        return
      end
    end

    user = User.new(signup_params)
    if user.save()
      case app
        when "pos"
          # TODO: Use user data
          if !Rails.env.production?
            body = {
              "rut_emisor":acepta_params[:dni_commerce],
              "razon_social":acepta_params[:business_name],
              "domicilio":acepta_params[:address],
              "correo":signup_params[:email],
              "giro": "Servicios",
              "ciudad":acepta_params[:city],
              "telefono":signup_params[:phone]
          }
          else 
            body = {
              "rut_emisor":"11111111-1",
              "razon_social":"Prueba",
              "domicilio":"Av Test 123",
              "correo":"prueba@acepta.com",
              "giro": "Servicios",
              "ciudad":"SANTIAGO",
              "telefono":"+56999999999"
          }
          end

          response = Faraday.post('https://servicios-cert.acepta.com/ms/ordename/crea_comercio') do |req|
            req.body = body.to_json
            req.headers['X-API-KEY'] = Rails.application.credentials[Rails.env.to_sym][:acepta][:api_key]
            req.headers['Content-Type'] = "application/json"
          end
        when "b2c"
          user_validation = UserValidation.find_by(phone: user.phone)
          user_validation.user_id = user.id
          user_validation.save
      end
      # TODO: return active token if any
      sign_in(user)
      render json: {
        sucess: true,
        message: "Done.",
        user: user,
        token: user.generate_jwt
      }
    else
      render json: {
        success: false,
        message: "User already exists.",
        data: nil
      },status: :unprocessable_entity
    end
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end


  # Step 1
  def send_validation_sms
    user_validation = UserValidation.find_by(phone: params["phone"])

    if !user_validation.nil?
      user_validation.validated_at = nil
      user_validation.save
    end

    send_sms(params["phone"])
  end

  # Step 2
  def validate_sms_phone
    begin
      user_validation = UserValidation.find_by_phone(validate_params)
      success_state = false
      message = ""

      if !user_validation.nil?
        if user_validation.validated_at.nil?
          if params["code"] == user_validation.sms_code
            if user_validation.expires_at >= Time.current
              user_validation.validated_at = Time.current
              user_validation.save

              success_state = true
              message = "Validated phone number."
            else
              message = "SMS Code expired at: #{user_validation.expires_at}"
            end
          else
            message = "SMS Code doesn't match."
          end
        else
          message = "Phone number has already been validated."
        end
      else
        message = "Phone number doesn't exists."
      end
    rescue Exception => e
      render json: {
        success: false,
        message: e.message,
        data: nil
      },status: :unprocessable_entity
      return
    end
    render json: {
      success: success_state,
      message: message,
      data: nil
    }
  end

  def send_sms(phone)
    begin
      code = Faker::Number.number(digits: 4).to_s
      expires_at = (Time.current + (UserValidation::SMS_EXPIRATION_TIME[:default]).minutes).strftime("%Y-%m-%d %H:%M:%S")
      message = ""

      # Creando registro de Validación de Celular
      user_validation = UserValidation.find_by(phone: phone)

      if user_validation
        message = "SMS Code was sent again."
        user_validation.sms_code = code
        user_validation.expires_at = expires_at
        user_validation.save
      else
        user_validation = UserValidation.create(
          :phone => phone,
          :sms_code => code,
          :expires_at => expires_at
        ).save
        message = "SMS Code was sent."
      end

      begin
        webservice = "https://api.connectus.cl/api_v2/send_sms"
        url = URI.parse(webservice)
        req = Net::HTTP::Post.new(url.request_uri, 'Content-Type' => 'application/json')
        api_id = "c264250d61934f5683cd4e21dc962b4d"
        sms_api = "c51744537b7a43ea8796f3258722213c"
        req.basic_auth api_id, sms_api
        req.body = {
          sms_content: "Hola!, tu código de Ordename es: #{code}\nEste código expirará dentro de #{UserValidation::SMS_EXPIRATION_TIME[:default]} minuto.",
          dst_number: phone,
          src_number: "56442349881"
        }.to_json

        # Envío de SMS
        res = Net::HTTP.start(url.host, url.port, use_ssl: true){ |http|
          http.open_timeout = 60
          http.read_timeout = 60
          http.request(req)
        }

        render json: {
          success: true,
          message: message,
          data: nil
        }

      rescue Exception => e
        render json: {
          success: false,
          message: e.message,
          data: nil
        },status: :unprocessable_entity
      end

    rescue Exception => e
      render json: {
        success: false,
        message: e.message,
        data: nil
      },status: :unprocessable_entity
    end
  end


  private
    def validate_params
      params.require([:phone])
    end

    def signup_params
      params.require(:data).permit(:dni_number, :password, :first_name, :last_names, :phone, :email)
    end

    def acepta_params
      params.require(:extra_data).permit(:dni_commerce, :city, :address, :business_name)
    end
end
