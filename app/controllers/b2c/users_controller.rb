class B2c::UsersController < B2c::B2cController
  before_action :authenticate_user
  before_action :set_user, only: [:show, :update]

  # > GET
  def show
    if !@user.nil?
      address = Address.where(user_id: @user).last
      render json: ResponseController::base_response.merge({
        user: @user,
        address: address
      })
    else
      render json: {
        success: false,
        message: "'User' not found."
      }.merge(ResponseController::nil_data)
    end
  end

  # > PATCH
  def update
    if !@user.nil?
      if @user.update(user_params)
        render json: @user
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    else
      render json: {
        success: false,
        message: "'User' not found."
      }.merge(ResponseController::nil_data)
    end
  end


  private
    def set_user
      begin
        @user = User.find(params[:id])
      rescue
        @user = nil
      end
    end

    def user_params
      params.require(:user).permit(:first_name, :last_names, :email, :phone)
    end
end