class B2c::PaymentsController < B2c::B2cController
  before_action :authenticate_user
  before_action :set_payment, only: [:show]

  def index
    render json: ResponseController::base_response.merge(payments: Payment.all)
  end

  def show
    if @payment.nil?
      @payment = ResponseController::nil_data
    end

    render json: ResponseController::base_response.merge(@payment.as_json)
  end


  def set_payment_order
    payment_order = Payment.new(payment_params)

    if payment_order.save!
      begin
        order = Order.find(params["order_id"])
      rescue
        render json: {
          success: false,
          message: "'Order' not found."
        }.merge(ResponseController::nil_data)
        return
      end

      order.payment_id = payment_order.id
      order.state = "payment"
      order.save!

      render json: ResponseController::base_response.merge(order.as_json)
    else
      render json: {
        success: false,
        message: "Failed."
      }.merge(ResponseController::nil_data)
    end
  end


  private
    def set_payment
      begin
        @payment = Payment.find(params[:id])
      rescue
        @payment = nil
      end
    end

    def payment_params
      params.permit(:state, :currency, :subtotal, :total, :payment_method_id)
    end
end
