class B2c::ProductsController < B2c::B2cController
  before_action :authenticate_user
  before_action :set_product, only: [:show, :product_detail]

  # > GET
  def index
    render json: ResponseController::base_response.merge(products: Product.all)
  end

  # > GET
  def show
    if @product.nil?
      @product = ResponseController::nil_data
    end

    render json: ResponseController::base_response.merge(@product.as_json)
  end

  # > GET
  # > Params > tag_params
  # > Products
  def get_by_tag
    productTags = ProductTag.where(tag_id: tag_params)
    products = Product.where(id: productTags)
    render json: products
  end

  # Búsquda de producto según texto
  def index_by_text
    products = Product.where("lower(name) like ?", "%#{params["name"].downcase}%")
    render json: ResponseController::base_response.merge(products: products.as_json)
  end

  def index_by_shop_text
    if Shop.exists?(id: params["shop_id"])
      products = Product.includes(:stocks).where(stocks: {shop_id: params["shop_id"]}).where("lower(name) like ?", "%#{params["name"].downcase}%")
      render json: ResponseController::base_response.merge(products: products.as_json)
    else
      render json: {
        success: false,
        message: "'Shop' not found."
      }.merge(ResponseController::nil_data)
    end
  end

  # Detalle del Producto de una Tienda
  def product_detail
    if !@product.nil?
      product_detail = Product.includes(stocks: :shop).where(id: @product.id, stocks: {shop_id: params["shop_id"]})
      render json: product_detail.to_json(
        :include => {
          :stocks => {
            :only => [:provider, :stock_01, :stock_02, :sku]
          }
        }
      )
    else
      render json: ResponseController::base_response.merge(ResponseController::nil_data)
    end
  end


  private
    def set_product
      begin
        @product = Product.find(params[:id])
      rescue
        @product = nil
      end
    end

    def tag_params
      params.require(:tag_id)
    end
end
