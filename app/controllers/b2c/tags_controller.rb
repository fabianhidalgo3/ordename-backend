class B2c::TagsController < B2c::B2cController
  before_action :authenticate_user

  # > GET
  def index
    render json: ResponseController::base_response.merge(tags: Tag.all)
  end
end
