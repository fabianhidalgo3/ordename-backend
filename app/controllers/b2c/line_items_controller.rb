class B2c::LineItemsController < B2c::B2cController
  before_action :authenticate_user
  before_action :set_line_items, only: [:show]

  # > GET
  def show
    if @line_items.nil?
      @line_items = ResponseController::nil_data
    end

    @line_items = @line_items.as_json(include: [:product])
    render json: ResponseController::base_response.merge(line_items: @line_items)
  end

  private
    def set_line_items
      begin
        @line_items = LineItem.where(order_id: params[:id])
      rescue
        @line_items = nil
      end
    end
end