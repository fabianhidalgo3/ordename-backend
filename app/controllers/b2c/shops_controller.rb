class B2c::ShopsController < B2c::B2cController
  before_action :authenticate_user

  def index
    shops = Shop.all
    detail = Shop.includes(:address).where(id: shops.ids)
    render json: ResponseController::base_response.merge(shops: detail.as_json(
      :include => {
        :address => {
          :only => [:address, :address2, :zipcode, :latitude, :longitude]
        }
      }
    ))
  end

  def show
    if Shop.exists?(id: params["id"])
      shop = Shop.includes(:address).where(id: params["id"]).first
      render json: ResponseController::base_response.merge(shop.as_json(
        :include => {
          :address => {
            :only => [:address, :address2, :zipcode, :latitude, :longitude]
          }
        }
      ))
    else
      render json: {
        success: false,
        message: "'Shop' not found."
      }.merge(ResponseController::nil_data)
    end
  end

  def nearest_for_user
    user_address = Address.find_by(user_id: @current_user_id)

    addresses = Address.near(
      [user_address.latitude, user_address.longitude],
      Address::RADIUS_IN_KILOMETERS[:default],
      units: :km
    )

    shops = []
    addresses.each do |address|
      if !Shop.find_by(address_id: address.id).nil?
        shop = Shop.find_by(address_id: address.id).as_json
        shop["address"] = address
        shops.append(shop)
      end
    end

    render json: {
      success: true,
      message: "Done.",
      shops: shops
    }
  end

  def products
    if Shop.exists?(id: params["id"])
      shop_products = Product.includes(:stocks).where(stocks: {shop_id: params["id"]})

      render json: ResponseController::base_response.merge(shops: shop_products.as_json)
    else
      render json: {
        success: false,
        message: "'Shop' not found."
      }.merge(ResponseController::nil_data)
    end
  end

  # Retorna todos los 'Tags' asociados a una 'Shop'
  def tags
    if Shop.exists?(id: params["id"])
      tag_list = Tag.joins(products: :shops).where(shops: {id: params["id"]}).uniq

      render json: ResponseController::base_response.merge(tags: tag_list.as_json)
    else
      render json: {
        success: false,
        message: "'Shop' not found."
      }.merge(ResponseController::nil_data)
    end
  end
end
