class B2c::PaymentMethodsController < B2c::B2cController
  before_action :authenticate_user
  before_action :set_payment_method, only: [:show]

  def index
    render json: ResponseController::base_response.merge(payment_methods: PaymentMethod.all)
  end

  def show
    if @payment_method.nil?
      @payment_method = ResponseController::nil_data
    end
    render json: ResponseController::base_response.merge(@payment_method.as_json)
  end


  private
    def set_payment_method
      begin
        @payment_method = PaymentMethod.find(params[:id])
      rescue
        @payment_method = nil
      end
    end
end
