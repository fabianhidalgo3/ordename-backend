class B2c::AddressesController < B2c::B2cController
  before_action :authenticate_user
  before_action :set_address, only: [:update]

  def index
    user_address = Address.where(user_id: @current_user_id).take.as_json

    if !user_address.nil?
      render json: ResponseController::base_response.merge(user_address)
    else
      render json: {
        success: false,
        message: "User hasn't address."
      }.merge(ResponseController::nil_data)
    end
  end

  def create
    address = Address.new(address_params)
    address.user_id = @current_user_id
    address.active = true
    address.save
    address = address.as_json

    render json: ResponseController::base_response.merge(address)
  end

  def update
    if !@address.nil?
      if @address.update(address_params)
        render json: @address
      else
        render json: @address.errors, status: :unprocessable_entity
      end
    else
      render json: {
        success: false,
        message: "'Address' not found."
      }.merge(ResponseController::nil_data)
    end
  end


  private
    def set_address
      begin
        @address = Address.find(params["id"])
      rescue
        @address = nil
      end
    end

    def address_params
      params.permit(:territory_id, :address, :address2, :latitude, :longitude, :zipcode, :active)
    end
end
