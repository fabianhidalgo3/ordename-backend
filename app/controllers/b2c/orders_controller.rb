class B2c::OrdersController < B2c::B2cController
  before_action :authenticate_user
  before_action :set_order, only: [:show, :cart, :checkout, :complete_user_order]

  def index
    render json: ResponseController::base_response.merge(orders: Order.all)
  end

  def show
    if @order.nil?
      @order = ResponseController::nil_data
    end

    render json: ResponseController::base_response.merge(@order.as_json)
  end


  # Productos de una 'Order'
  def cart
    if !@order.nil?
      order_products = Order.includes(line_items: :product).where(id: @order.id, state: 'cart')
      render json: ResponseController::base_response.merge(orders: order_products.as_json(
        :include => {
          :line_items => {
            :include => {
              :product => {
                :only => [:name]
              }
            }, :only => [:quantity, :unit, :unit_price, :state, :total, :payed]
          }
        }
      ))
    else
      render json: ResponseController::base_response.merge(ResponseController::nil_data)
    end
  end

  # TODO: Agregar Payment
  def checkout
    if !@order.nil?
      order_products = Order.includes({:line_items => :product}, {buyer_shop: :address}).where(id: @order.id, state: 'payment')
      render json: ResponseController::base_response.merge(orders: order_products.as_json(
        :include => {
          :line_items => {
            :include => {
              :product => {
                :only => [:name]
              }
            }, :only => [:quantity, :unit, :unit_price, :state, :total, :payed]
          },
          :buyer_shop =>{
            :include => {
              :address =>{
                :only => [:address, :address2, :latitude, :longitude]
              }
            },
            :only => [:name]
          },
        }
      ))
    else
      render json: ResponseController::base_response.merge(ResponseController::nil_data)
    end
  end

  def set_user_order
    order = Order.new(order_params)
    order.user_id = @current_user_id
    order.state = "cart"

    if order.save!
      params["products"].each do |product|
        line_item = LineItem.new(
          "order_id": order.id,
          "product_id": product["product_id"],
          "seller_id": order.buyer_shop_id,
          "quantity": product["quantity"],
          "unit": product["unit"],
          "transformation": product["transformation"],
          "unit_price": product["unit_price"],
          "state": product["state"],
          "total": product["total"],
          "payed": product["payed"],
        )
        if !line_item.save
          render json: {
            success: false,
            message: "Failed on 'Line Item'."
          }.merge(ResponseController::nil_data)
          return
        end
      end

      render json: ResponseController::base_response.merge(order.as_json)
    else
      render json: {
        success: false,
        message: "Failed on 'Order'."
      }.merge(ResponseController::nil_data)
    end
  end

  def complete_user_order
    @order.state = "complete"

    if @order.save
      render json: ResponseController::base_response.merge(@order.as_json)
    else
      render json: {
        success: false,
        message: "Failed."
      }.merge(ResponseController::nil_data)
    end
  end

  # Historial de Órdenes según un Usuario
  def order_history
    orders = Order.where(user_id: @current_user_id).order(created_at: :asc)
    if orders.nil?
      orders = ResponseController::nil_data
    end

    render json: ResponseController::base_response.merge(orders: orders)
  end


  private
    def set_order
      begin
        @order = Order.find(params["id"])
      rescue
        @order = nil
      end
    end

    def order_params
      params.require(:order).permit(:buyer_shop_id, :document_id, :subtotal, :currency, :total_taxes, :total_discount, :total)
    end
end
