class ResponseController < ActiveRecord::Base
  def self.base_response
    {
      success: true,
      message: "Done."
    }
  end

  def self.nil_data
    {
      data: nil
    }
  end
end