require 'faker'

p "Users, Business, Employee"
2.times do
 user =  User.create(
  :dni_number => Faker::ChileRut.full_rut, 
  :first_name => Faker::Name.name,
  :last_names => Faker::Name.last_name ,
  :phone => Faker::PhoneNumber.cell_phone_in_e164, 
  :email => Faker::Internet.email,
  :password => "123456")
  business =  Business.create(:rut => Faker::ChileRut.full_rut,:name => Faker::Name.name)
  employee = Employee.create(user_id: user.id,business_id: business.id,role: 'b2c')
end
@users = User.all
@business = Business.all

p "Country"
country =  Country.create(
  name: Faker::Address.country,
  geojson: 
  {
    "type": "FeatureCollection",
    "features": [
      {
        "type": "Feature",
        "properties": {},
        "geometry": {
          "type": "Point",
          "coordinates": [
            -72.421875,
            -37.387617499783936
          ]
        }
      }
    ]
  }
)

p "TerritoryType"
territoryType1 = TerritoryType.create(name: "Región", country_id: country.id )
territoryType2 = TerritoryType.create(name: "Provincia", country_id: country.id )
territoryType3 = TerritoryType.create(name: "Comuna", country_id: country.id )

p "Territory"
territory1 = Territory.create(territory_type_id: territoryType1.id,name: 'Valparaiso')
territory2 = Territory.create(territory_type_id: territoryType2.id,name: 'Viña del Mar')
territory3 = Territory.create(territory_type_id: territoryType3.id,name: 'Con Con')

p "Address"
2.times do
  Address.create(
    user_id: @users.first.id,
    territory_id: territory1.id,
    zipcode: Faker::Address.zip_code,
    active: 1,
    address: Faker::Address.street_address,
    address2: Faker::Address.secondary_address,
    latitude: Faker::Address.latitude,
    longitude: Faker::Address.longitude,
  )
end
2.times do
  Address.create(
    user_id: @users.second.id,
    territory_id: territory2.id,
    zipcode: Faker::Address.zip_code,
    active: 1,
    address: Faker::Address.street_address,
    address2: Faker::Address.secondary_address,
    latitude: Faker::Address.latitude,
    longitude: Faker::Address.longitude,
  )
end

p "Shop"
@address = Address.all

@business.each do |bus|
  @address.each do |add|
    Shop.create(
      address_id: add.id,
      business_id: bus.id,
      sort_value: 1,
      name: Faker::Company.name,
      description:Faker::Company.industry ,
      min_purchase_amount: Faker::Number.decimal(l_digits: 3, r_digits: 3),
      currency: Faker::Currency.code,
      provider: false
    )
  end
end
@shop = Shop.all

@territories = Territory.all
@shop.each do |shop|
  @territories.each do |territorio|
    ShopCoverageTerritory.create(
      territory_id: territorio.id,
      shop_id: shop.id,
      red_zone: false
    )
  end
end


p "Products"
2.times do
  Product.create(
    :name => Faker::Commerce.product_name,
    :sort_value => Faker::Number.number(digits: 1),
    :ean => Faker::Code.ean,
    :dun => Faker::Number.number(digits: 8),
    :rtu => Faker::Number.number(digits: 8),
    :unit_01 => Faker::Alphanumeric.alpha(number: 5),
    :unit_02 => Faker::Alphanumeric.alpha(number: 5)
  )
end

p "Tax"
2.times do
  Tax.create(
    :name => Faker::Name.name,
    :percent => Faker::Number.decimal(l_digits: 2, r_digits: 3)
  )
end

@products = Product.all

p "Product Taxes"
@products.each do |product|
  @taxes = Tax.all do |tax|
    ProductTax.create(
      :product_id => product.id,
      :tax_id => tax.id)
  end
end

p "Tags"
Tag.create(
  :sort_value => Faker::Number.number(digits: 2),
  :name => "Bebestibles",
  :tag_type => "type_1"
)
Tag.create(
  :sort_value => Faker::Number.number(digits: 2),
  :name => "Despensa",
  :tag_type => "type_2"
)
Tag.create(
  :sort_value => Faker::Number.number(digits: 2),
  :name => "Lácteos",
  :tag_type => "type_3"
)
Tag.create(
  :sort_value => Faker::Number.number(digits: 2),
  :name => "Limpieza",
  :tag_type => "type_4"
)
Tag.create(
  :sort_value => Faker::Number.number(digits: 2),
  :name => "Confites",
  :tag_type => "type_5"
)

p "Product Tags"
@products.each do |product|
  Tag.all.each do |tag|
    ProductTag.create(
      :product_id => product.id,
      :tag_id => tag.id
    )
  end
end


p "Stocks"
@products.each do |product|
  Shop.all.each do |shop|
    Stock.create(
      :shop_id => shop.id,
      :product_id => product.id,
      :provider => Faker::Boolean.boolean(true_ratio: 0.75),
      :stock_01 => Faker::Number.number(digits: 2),
      :stock_02 => Faker::Number.number(digits: 4),
      :sku => Faker::Code.sin
    )
  end
end


p "Prices"
@products.each do |product|
  Shop.all.each do |shop|
    Price.create(
      :seller_id => shop.id,
      :buyer_shop_id => shop.id,
      :product_id => product.id,
      :buyer_user_id => nil,
      :territory_id => nil,
      :min_quantity => Faker::Number.number(digits: 1),
      :max_quantity => Faker::Number.number(digits: 2),
      :max_desc => Faker::Number.decimal(l_digits: 4, r_digits: 2)
    )
  end
end

p "Order"
@users.each do |user|
  Shop.all.each do |shop|
    Address.all.each do |add|
      Order.create(
        seller_user_id: user.id,
        seller_shop_id: shop.id,
        address_id: add.id,
        document_id: "1",
        state: "cart",
        subtotal: Faker::Number.decimal(l_digits: 4, r_digits: 2),
        currency: Faker::Currency.code,
        total_taxes: Faker::Number.decimal(l_digits: 4, r_digits: 2),
        total_discount: Faker::Number.decimal(l_digits: 4, r_digits: 2),
        total:Faker::Number.decimal(l_digits: 4, r_digits: 2),
      )
    end
  end
end

p "LineItem"
Order.all.each do |orden|
  Product.all.each do |product|
    Shop.all.each do |shop|
      LineItem.create(
        order_id: orden.id,
        product_id: product.id,
        seller_id: shop.id,
        quantity: Faker::Number.number(digits: 1),
        unit: Faker::Number.number(digits: 2),
        transformation: "Transformacion",
        unit_price: Faker::Number.decimal(l_digits: 4, r_digits: 2),
        state: "Vigente",
        total: Faker::Number.decimal(l_digits: 4, r_digits: 2),
        payed: Faker::Number.decimal(l_digits: 4, r_digits: 2),
      )
    end
  end
end

p "PaymentMethod"
PaymentMethod.create(
  name: "Efectivo",
  description: "Pago con dinero en físico",
  active: true,
  display_on: "Cart"
)

p "Fin"
