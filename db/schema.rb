# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_05_11_140513) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "territory_id", null: false
    t.string "zipcode"
    t.boolean "active"
    t.string "address"
    t.string "address2"
    t.float "latitude"
    t.float "longitude"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["territory_id"], name: "index_addresses_on_territory_id"
    t.index ["user_id"], name: "index_addresses_on_user_id"
  end

  create_table "businesses", force: :cascade do |t|
    t.string "rut"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "countries", force: :cascade do |t|
    t.string "name"
    t.json "geojson"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "coupon_gifts", force: :cascade do |t|
    t.bigint "promotion_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["promotion_id"], name: "index_coupon_gifts_on_promotion_id"
  end

  create_table "employees", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "business_id", null: false
    t.string "role"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["business_id"], name: "index_employees_on_business_id"
    t.index ["user_id"], name: "index_employees_on_user_id"
  end

  create_table "forbidden_shops", force: :cascade do |t|
    t.bigint "buyer_id", null: false
    t.bigint "seller_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["buyer_id"], name: "index_forbidden_shops_on_buyer_id"
    t.index ["seller_id"], name: "index_forbidden_shops_on_seller_id"
  end

  create_table "forbidden_users", force: :cascade do |t|
    t.bigint "buyer_id", null: false
    t.bigint "seller_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["buyer_id"], name: "index_forbidden_users_on_buyer_id"
    t.index ["seller_id"], name: "index_forbidden_users_on_seller_id"
  end

  create_table "line_items", force: :cascade do |t|
    t.bigint "order_id", null: false
    t.bigint "product_id", null: false
    t.bigint "seller_id", null: false
    t.integer "quantity"
    t.string "unit"
    t.integer "transformation"
    t.float "unit_price"
    t.string "state"
    t.float "total"
    t.float "payed"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["order_id"], name: "index_line_items_on_order_id"
    t.index ["product_id"], name: "index_line_items_on_product_id"
    t.index ["seller_id"], name: "index_line_items_on_seller_id"
  end

  create_table "operations", force: :cascade do |t|
    t.string "name"
    t.string "entity"
    t.string "attribute"
    t.bigint "operator_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["operator_id"], name: "index_operations_on_operator_id"
  end

  create_table "operators", force: :cascade do |t|
    t.string "name"
    t.string "input_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "order_promotions", force: :cascade do |t|
    t.bigint "order_id", null: false
    t.bigint "promotion_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["order_id"], name: "index_order_promotions_on_order_id"
    t.index ["promotion_id"], name: "index_order_promotions_on_promotion_id"
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "seller_user_id", null: false
    t.bigint "seller_shop_id", null: false
    t.bigint "buyer_user_id", null: false
    t.bigint "buyer_shop_id", null: false
    t.string "document_id"
    t.string "state"
    t.float "subtotal"
    t.string "currency"
    t.float "total_taxes"
    t.float "total_discount"
    t.float "total"
    t.bigint "address_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "payment_id"
    t.index ["address_id"], name: "index_orders_on_address_id"
    t.index ["buyer_shop_id"], name: "index_orders_on_buyer_shop_id"
    t.index ["buyer_user_id"], name: "index_orders_on_buyer_user_id"
    t.index ["payment_id"], name: "index_orders_on_payment_id"
    t.index ["seller_shop_id"], name: "index_orders_on_seller_shop_id"
    t.index ["seller_user_id"], name: "index_orders_on_seller_user_id"
  end

  create_table "payment_methods", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.boolean "active"
    t.string "display_on"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "payments", force: :cascade do |t|
    t.string "state"
    t.string "currency"
    t.float "subtotal"
    t.float "total"
    t.bigint "payment_method_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["payment_method_id"], name: "index_payments_on_payment_method_id"
  end

  create_table "prices", force: :cascade do |t|
    t.bigint "seller_id", null: false
    t.bigint "buyer_shop_id"
    t.bigint "buyer_user_id"
    t.bigint "product_id", null: false
    t.bigint "territory_id"
    t.integer "min_quantity"
    t.integer "max_quantity"
    t.float "max_desc"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["buyer_shop_id"], name: "index_prices_on_buyer_shop_id"
    t.index ["buyer_user_id"], name: "index_prices_on_buyer_user_id"
    t.index ["product_id"], name: "index_prices_on_product_id"
    t.index ["seller_id"], name: "index_prices_on_seller_id"
    t.index ["territory_id"], name: "index_prices_on_territory_id"
  end

  create_table "product_discounts", force: :cascade do |t|
    t.bigint "product_id", null: false
    t.string "product_discount_type"
    t.float "amount"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["product_id"], name: "index_product_discounts_on_product_id"
  end

  create_table "product_gifts", force: :cascade do |t|
    t.bigint "product_id", null: false
    t.integer "quantity"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["product_id"], name: "index_product_gifts_on_product_id"
  end

  create_table "product_tags", force: :cascade do |t|
    t.bigint "tag_id", null: false
    t.bigint "product_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["product_id"], name: "index_product_tags_on_product_id"
    t.index ["tag_id"], name: "index_product_tags_on_tag_id"
  end

  create_table "product_taxes", force: :cascade do |t|
    t.bigint "product_id", null: false
    t.bigint "tax_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["product_id"], name: "index_product_taxes_on_product_id"
    t.index ["tax_id"], name: "index_product_taxes_on_tax_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.integer "sort_value"
    t.string "ean"
    t.string "dun"
    t.integer "rtu"
    t.string "unit_01"
    t.string "unit_02"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "promotions", force: :cascade do |t|
    t.bigint "shop_id"
    t.string "code"
    t.string "name"
    t.string "description"
    t.boolean "public"
    t.boolean "active"
    t.integer "uses"
    t.integer "max_uses"
    t.integer "max_uses_per_user"
    t.bigint "action_id"
    t.string "action_type"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.boolean "cumulative"
    t.string "match_policy"
    t.integer "sort_value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["action_id", "action_type"], name: "index_promotions_on_action_id_and_action_type"
    t.index ["shop_id"], name: "index_promotions_on_shop_id"
  end

  create_table "rules", force: :cascade do |t|
    t.bigint "promotion_id", null: false
    t.string "name"
    t.boolean "negative"
    t.bigint "operation_id", null: false
    t.string "value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["operation_id"], name: "index_rules_on_operation_id"
    t.index ["promotion_id"], name: "index_rules_on_promotion_id"
  end

  create_table "shipment_discounts", force: :cascade do |t|
    t.string "shipment_discount_type"
    t.float "amount"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "shop_coverage_territories", force: :cascade do |t|
    t.bigint "territory_id", null: false
    t.bigint "shop_id", null: false
    t.boolean "red_zone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["shop_id"], name: "index_shop_coverage_territories_on_shop_id"
    t.index ["territory_id"], name: "index_shop_coverage_territories_on_territory_id"
  end

  create_table "shops", force: :cascade do |t|
    t.bigint "address_id", null: false
    t.bigint "business_id", null: false
    t.integer "sort_value"
    t.string "name"
    t.string "description"
    t.float "min_purchase_amount"
    t.string "currency"
    t.boolean "provider"
    t.string "contact_phone"
    t.string "instagram"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["address_id"], name: "index_shops_on_address_id"
    t.index ["business_id"], name: "index_shops_on_business_id"
  end

  create_table "stocks", force: :cascade do |t|
    t.bigint "shop_id", null: false
    t.bigint "product_id", null: false
    t.boolean "provider"
    t.integer "stock_01"
    t.integer "stock_02"
    t.integer "sku"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["product_id"], name: "index_stocks_on_product_id"
    t.index ["shop_id"], name: "index_stocks_on_shop_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.string "tag_type"
    t.integer "sort_value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "parent_id"
    t.index ["parent_id"], name: "index_tags_on_parent_id"
  end

  create_table "taxes", force: :cascade do |t|
    t.string "name"
    t.float "percent"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "territories", force: :cascade do |t|
    t.bigint "territory_type_id"
    t.string "name"
    t.json "geojson"
    t.integer "sort_value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "parent_id"
    t.index ["parent_id"], name: "index_territories_on_parent_id"
    t.index ["territory_type_id"], name: "index_territories_on_territory_type_id"
  end

  create_table "territory_types", force: :cascade do |t|
    t.string "name"
    t.bigint "country_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "parent_id"
    t.index ["country_id"], name: "index_territory_types_on_country_id"
    t.index ["parent_id"], name: "index_territory_types_on_parent_id"
  end

  create_table "user_promotions", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "promotion_id", null: false
    t.integer "uses"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["promotion_id"], name: "index_user_promotions_on_promotion_id"
    t.index ["user_id"], name: "index_user_promotions_on_user_id"
  end

  create_table "user_validations", force: :cascade do |t|
    t.string "phone", null: false
    t.string "sms_code", null: false
    t.datetime "validated_at"
    t.datetime "expires_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id"
    t.index ["phone"], name: "index_user_validations_on_phone", unique: true
    t.index ["user_id"], name: "index_user_validations_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "dni_number"
    t.string "first_name", null: false
    t.string "last_names", null: false
    t.string "phone", default: "", null: false
    t.string "firebase_token"
    t.string "hub_token", default: ""
    t.boolean "pos_accepted", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.index ["dni_number"], name: "index_users_on_dni_number", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "addresses", "territories"
  add_foreign_key "addresses", "users"
  add_foreign_key "coupon_gifts", "promotions"
  add_foreign_key "employees", "businesses"
  add_foreign_key "employees", "users"
  add_foreign_key "forbidden_shops", "shops", column: "buyer_id"
  add_foreign_key "forbidden_shops", "shops", column: "seller_id"
  add_foreign_key "forbidden_users", "shops", column: "seller_id"
  add_foreign_key "forbidden_users", "users", column: "buyer_id"
  add_foreign_key "line_items", "orders"
  add_foreign_key "line_items", "products"
  add_foreign_key "line_items", "shops", column: "seller_id"
  add_foreign_key "operations", "operators"
  add_foreign_key "order_promotions", "orders"
  add_foreign_key "order_promotions", "promotions"
  add_foreign_key "orders", "addresses"
  add_foreign_key "orders", "payments"
  add_foreign_key "orders", "shops", column: "buyer_shop_id"
  add_foreign_key "orders", "shops", column: "seller_shop_id"
  add_foreign_key "orders", "users", column: "buyer_user_id"
  add_foreign_key "orders", "users", column: "seller_user_id"
  add_foreign_key "prices", "products"
  add_foreign_key "prices", "shops", column: "buyer_shop_id"
  add_foreign_key "prices", "shops", column: "seller_id"
  add_foreign_key "prices", "territories"
  add_foreign_key "prices", "users", column: "buyer_user_id"
  add_foreign_key "product_discounts", "products"
  add_foreign_key "product_gifts", "products"
  add_foreign_key "product_tags", "products"
  add_foreign_key "product_tags", "tags"
  add_foreign_key "product_taxes", "products"
  add_foreign_key "product_taxes", "taxes"
  add_foreign_key "promotions", "shops"
  add_foreign_key "rules", "operations"
  add_foreign_key "rules", "promotions"
  add_foreign_key "shop_coverage_territories", "shops"
  add_foreign_key "shop_coverage_territories", "territories"
  add_foreign_key "shops", "addresses"
  add_foreign_key "shops", "businesses"
  add_foreign_key "stocks", "products"
  add_foreign_key "stocks", "shops"
  add_foreign_key "tags", "tags", column: "parent_id"
  add_foreign_key "territories", "territories", column: "parent_id"
  add_foreign_key "territories", "territory_types"
  add_foreign_key "territory_types", "countries"
  add_foreign_key "territory_types", "territory_types", column: "parent_id"
  add_foreign_key "user_promotions", "promotions"
  add_foreign_key "user_promotions", "users"
  add_foreign_key "user_validations", "users"
end
