class CreateForbiddenUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :forbidden_users do |t|
      t.references :buyer, null: false, foreign_key: { to_table: :users }
      t.references :seller, null: false, foreign_key: { to_table: :shops }

      t.timestamps
    end
  end
end
