class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :name
      t.integer :sort_value
      t.string :ean
      t.string :dun
      t.integer :rtu
      t.string :unit_01
      t.string :unit_02

      t.timestamps
    end
  end
end
