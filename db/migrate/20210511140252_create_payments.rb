class CreatePayments < ActiveRecord::Migration[6.0]
  def change
    create_table :payments do |t|
      t.string :state
      t.string :currency
      t.float :subtotal
      t.float :total
      t.references :payment_method

      t.timestamps
    end

    add_reference :orders, :payment, null: true, foreign_key: true
  end
end
