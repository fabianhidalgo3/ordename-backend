class CreateAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :addresses do |t|
      t.references :user, null: false, foreign_key: true
      t.references :territory, null: false, foreign_key: true
      t.string :zipcode
      t.boolean :active
      t.string :address
      t.string :address2
      t.float :latitude
      t.float :longitude
      t.timestamp :deleted_at

      t.timestamps
    end
  end
end
