class AddParentToTerritoryTypes < ActiveRecord::Migration[6.0]
  def change
    add_reference :territory_types, :parent, foreign_key: { to_table: :territory_types }
  end
end
