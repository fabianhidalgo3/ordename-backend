class CreateTerritoryTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :territory_types do |t|
      t.string :name
      t.references :country, null: false, foreign_key: true

      t.timestamps
    end
  end
end
