class CreateTerritories < ActiveRecord::Migration[6.0]
  def change
    create_table :territories do |t|
      t.references :territory_type, foreign_key: { to_table: :territory_types }
      t.string :name
      t.json :geojson
      t.integer :sort_value

      t.timestamps
    end
  end
end
