class CreateUserValidations < ActiveRecord::Migration[6.0]
  def change
    create_table :user_validations do |t|
      t.string :phone, null: false
      t.string :sms_code, null: false
      t.datetime :validated_at
      t.datetime :expires_at

      t.timestamps
    end
    add_index :user_validations, :phone, unique: true

    add_reference :user_validations, :user, null: true, foreign_key: true
  end
end
