class CreateShipmentDiscounts < ActiveRecord::Migration[6.0]
  def change
    create_table :shipment_discounts do |t|
      t.string :shipment_discount_type
      t.float :amount

      t.timestamps
    end
  end
end
