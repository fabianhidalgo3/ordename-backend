class CreateOperations < ActiveRecord::Migration[6.0]
  def change
    create_table :operations do |t|
      t.string :name
      t.string :entity
      t.string :attribute
      t.references :operator, null: false, foreign_key: true

      t.timestamps
    end
  end
end
