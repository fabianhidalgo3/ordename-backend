class CreateBusinesses < ActiveRecord::Migration[6.0]
  def change
    create_table :businesses do |t|
      t.string :rut
      t.string :name

      t.timestamps
    end
  end
end
