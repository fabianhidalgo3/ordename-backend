class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :dni_number
      t.string :first_name, null: false
      t.string :last_names, null: false
      t.string :phone, null: false, default: ""
      t.string :firebase_token
      t.string :hub_token, default: ""
      t.boolean :pos_accepted, default: false
      
      t.timestamps
    end
  end
end
