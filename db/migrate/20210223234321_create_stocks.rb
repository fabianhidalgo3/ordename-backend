class CreateStocks < ActiveRecord::Migration[6.0]
  def change
    create_table :stocks do |t|
      t.references :shop, null: false, foreign_key: true
      t.references :product, null: false, foreign_key: true
      t.boolean :provider
      t.integer :stock_01
      t.integer :stock_02
      t.integer :sku

      t.timestamps
    end
  end
end
