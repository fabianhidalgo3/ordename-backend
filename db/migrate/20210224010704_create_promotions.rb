class CreatePromotions < ActiveRecord::Migration[6.0]
  def change
    create_table :promotions do |t|
      t.references :shop, foreign_key: true
      t.string :code
      t.string :name
      t.string :description
      t.boolean :public
      t.boolean :active
      t.integer :uses
      t.integer :max_uses
      t.integer :max_uses_per_user
      t.bigint :action_id
      t.string :action_type
      t.datetime :starts_at
      t.datetime :ends_at
      t.boolean :cumulative
      t.string :match_policy
      t.integer :sort_value

      t.timestamps
    end

    # polymorphic association
    add_index :promotions, [:action_id, :action_type]
  end
end
