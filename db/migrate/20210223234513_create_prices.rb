class CreatePrices < ActiveRecord::Migration[6.0] 
  def change
    create_table :prices do |t|
      t.references :seller, null: false, foreign_key: { to_table: :shops }
      t.references :buyer_shop, foreign_key: { to_table: :shops }
      t.references :buyer_user, foreign_key: { to_table: :users }
      t.references :product, null: false, foreign_key: true
      t.references :territory, foreign_key: true
      t.integer :min_quantity
      t.integer :max_quantity
      t.float :max_desc

      t.timestamps
    end
  end
end
