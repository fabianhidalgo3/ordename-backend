class CreateRules < ActiveRecord::Migration[6.0]
  def change
    create_table :rules do |t|
      t.references :promotion, null: false, foreign_key: true
      t.string :name
      t.boolean :negative
      t.references :operation, null: false, foreign_key: true
      t.string :value

      t.timestamps
    end
  end
end
