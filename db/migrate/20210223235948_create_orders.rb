class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.references :seller_user, null: true, foreign_key: { to_table: :users}
      t.references :seller_shop, null: true, foreign_key: { to_table: :shops }
      t.references :buyer_user, null: true, foreign_key: { to_table: :users }
      t.references :buyer_shop, null: true, foreign_key: { to_table: :shops }
      t.string :document_id
      t.string :state
      t.float :subtotal
      t.string :currency
      t.float :total_taxes
      t.float :total_discount
      t.float :total
      t.references :address, null: true, foreign_key: true

      t.timestamps
    end
  end
end
