class AddParentToTerritories < ActiveRecord::Migration[6.0]
  def change
    add_reference :territories, :parent, foreign_key: { to_table: :territories }
  end
end
