class CreateProductDiscounts < ActiveRecord::Migration[6.0]
  def change
    create_table :product_discounts do |t|
      t.references :product, null: false, foreign_key: true
      t.string :product_discount_type
      t.float :amount

      t.timestamps
    end
  end
end
