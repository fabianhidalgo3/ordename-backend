class CreateShops < ActiveRecord::Migration[6.0]
  def change
    create_table :shops do |t|
      t.references :address, null: false, foreign_key: true
      t.references :business, null: false, foreign_key: true
      t.integer :sort_value
      t.string :name
      t.string :description
      t.float :min_purchase_amount
      t.string :currency
      t.boolean :provider
      t.string :contact_phone
      t.string :instagram
      t.string :image_url

      t.timestamps
    end
  end
end
