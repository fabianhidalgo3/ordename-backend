class CreateCouponGifts < ActiveRecord::Migration[6.0]
  def change
    create_table :coupon_gifts do |t|
      t.references :promotion, null: false, foreign_key: true

      t.timestamps
    end
  end
end
