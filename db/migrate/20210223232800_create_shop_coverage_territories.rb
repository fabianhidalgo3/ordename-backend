class CreateShopCoverageTerritories < ActiveRecord::Migration[6.0]
  def change
    create_table :shop_coverage_territories do |t|
      t.references :territory, null: false, foreign_key: true
      t.references :shop, null: false, foreign_key: true
      t.boolean :red_zone

      t.timestamps
    end
  end
end
