class CreateLineItems < ActiveRecord::Migration[6.0]
  def change
    create_table :line_items do |t|
      t.references :order, null: false, foreign_key: true
      t.references :product, null: false, foreign_key: true
      t.references :seller, null: false, foreign_key: { to_table: :shops }
      t.integer :quantity
      t.string :unit
      t.integer :transformation
      t.float :unit_price
      t.string :state
      t.float :total
      t.float :payed

      t.timestamps
    end
  end
end
