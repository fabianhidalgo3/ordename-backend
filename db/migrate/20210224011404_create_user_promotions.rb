class CreateUserPromotions < ActiveRecord::Migration[6.0]
  def change
    create_table :user_promotions do |t|
      t.references :user, null: false, foreign_key: true
      t.references :promotion, null: false, foreign_key: true
      t.integer :uses

      t.timestamps
    end
  end
end
