Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    passwords: "users/passwords"
  }

  devise_scope :user do
    post 'users/send_validation_sms' => 'users/registrations#send_validation_sms'
    post 'users/validate_sms_phone' => 'users/registrations#validate_sms_phone'
  end

  scope(:path => '/pos') do

    post '/login_random', to: 'pos#login_random', :defaults => { :format => 'json' }
    post '/create_order', to: 'pos#create_order', :defaults => { :format => 'json'}

    post '/dashboard_info', to: 'pos#get_dashboard_info', :defaults => {:format => 'json'}
    post '/get_orders', to: 'pos#get_orders', :defaults => {:format => 'json'}
    post '/get_order', to: 'pos#get_order', :defaults => {:format => 'json'}

    scope(:path => '/users') do
      get '/check_application', to: 'pos#user_accepted', :defaults => {:format => 'json'}
      post '/confirm_application', to: 'pos#accept_user', :defaults => {:format => 'json'}
    end
  end

  namespace :b2c , :defaults => {:format => :json} do
    resources :addresses, except: [:new, :edit]

    resources :line_items, except: [:new, :edit]

    resources :orders, only: [:index, :show] do
      collection do
        get 'order_history'
        post 'set_user_order'
      end
      member do
        get 'cart'
        post 'checkout'
        put 'complete_user_order'
      end
    end

    resources :payments, only: [:index, :show] do
      collection do
        post 'set_payment_order'
      end
    end

    resources :payment_methods, only: [:index, :show]

    resources :products, except: [:new, :edit] do
      collection do
        get 'get_by_tag'
        post 'index_by_text'
        post 'index_by_shop_text'
      end
      member do
        post 'product_detail'
      end
    end

    resources :shops, only: [:index, :show] do
      collection do
        get 'nearest_for_user'
      end
      member do
        get 'products'
        get 'tags'
      end
    end

    resources :tags, only: [:index] do
    end

    resources :users, except: [:new, :edit]

  end
end
